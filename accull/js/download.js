
var info = {
    x64: {keywords: ['win64', 'wow64'],
          desc: '2.7.2 for Windows 64-bit with Mercurial 2.5.4',
          url: ''},
    linux2: {keywords: ['linux', 'sunos', 'hp-ux'],
             desc: '2.7.2 for Linux',
             url: ''},
    darwin: {keywords: ['mac'],
             desc: '2.7.2 for Mac OS X',
             url: ''}}

var pf = window.navigator.platform.toLowerCase();
var ua = window.navigator.userAgent.toLowerCase();
for (var name in info) {
    var data = info[name];
    var match = false;
    for (var j in data.keywords) {
        var kw = data.keywords[j];
        if (pf.indexOf(kw) != -1 || ua.indexOf(kw) != -1) {
            match = true;
            break;
        }
    }
    if (match) {
        $('.dlButton > span').text(data.desc);
        $('.dlButton').attr('href', data.url);
        if (name == 'x64') {
            $('#altLink').text('32-bit')
                         .attr('href', '');
        } else if (name == 'linux2' || name == 'darwin') {
            $('#altLink').hide();
        }
        break;
    }
}
