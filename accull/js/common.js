/* *** Language Selector *** */

$('.headerWrapper select').change(function(){
    var url = 'http://tortoisehg.org/';
    var lang = $(this).val();
    if (lang != 'en') {
        url += lang + '/';
    }
    window.location.href = url;
});