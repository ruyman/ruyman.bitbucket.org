/* *** Visual Tour *** */

// init
$('.imagesWrapper > a > img').hide();
$('.imagesWrapper').css('display', 'block');

// wait for loading all images
jQuery.event.add(window, 'load', function(){

// resize all images
var MAX_W = 380, MAX_H = 280;
$('.imagesWrapper > a > img').each(function(){
    var img = $(this);
    if (img.width() > MAX_W)
        img.width(MAX_W);
    if (img.height() > MAX_H){
        img.width('');
        img.height(MAX_H);
    }
});

// define switcher
var frame = $('.visualTour');
var caption = $('.captionWrapper');
var first = $('.imagesWrapper > a:first').children().first();
var FRAME_W = frame.width(), FRAME_H = frame.height();
var switching = function(){
    var cur = $('.imagesWrapper > a > img:visible');
    var next = cur.parent().next('a').children().first();
    if (next.size() == 0)
        next = first;
    cur.fadeOut(1000);
    var pos = {left: (FRAME_W - next.width()) / 2,
        top: (FRAME_H - next.height()) / 2};
    next.css(pos);
    caption
        .fadeOut(700, function(){
            var top = pos.top + next.height();
            if (4 * caption.outerHeight() < next.height())
                top -= caption.outerHeight();
            $(this)
                .text(next.attr('alt'))
                .css({left: pos.left, width: next.width(), top: top})
                .fadeIn(1000)});
    next.fadeIn(1000);
};

// hide static image
setTimeout(function(){
    $('.coverImage img').fadeOut(400, function(){
        $('.coverImage').hide();
    });
}, 800);

// run switcher
setTimeout(function(){
    switching();
    setTimeout(arguments.callee, 6000);
}, 0);

});